import { createMuiTheme } from '@material-ui/core/styles';


const theme = createMuiTheme({
    color: {
      primary: "#1976D2",
      secondary: "#2196F3",
    }
  });


  export default theme
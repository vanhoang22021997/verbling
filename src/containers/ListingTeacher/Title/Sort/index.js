/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";

import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import styles from "./styles";
import Checkbox from "@material-ui/core/Checkbox";
import { Typography } from '@material-ui/core';
import clsx from "clsx";
import  './style.css'


const sortList = ["Online Now", "Favorites"]

const sortListByMagic = [
  {
    label: "Sort By Magic",
    status: true
  },
  {
    label: "Sort by Most Popular",
    status: false
  },
  {
    label: "Sort by Price Low-High",
    status: false
  },
  {
    label: "Sort by Price High-Low",
    status: false
  },
  {
    label: "Sort by Most Recently Active",
    status: false
  }
];

class Sort extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: -1,
      showMenuSort: false
    };
  }

  handleChange = index => event => {
    this.setState({
      checked: index
    });
  };

  showSort = (sortList)=>{
    const { classes } = this.props
    const { checked } = this.state;
    let result = null
    if(sortList.length > 0){
      result = sortList.map ( (item,index) => {
        return (
          <div key={index}>
          <Checkbox
            checked={checked === index}
            onChange={this.handleChange(index)}
            value="checked"
            inputProps={{
              "aria-label": "secondary checkbox"
            }}
            classes={{ root: classes.checkbox}}
            
          />
          <span className={classes.textCheckbox}>{item}</span>
        </div>
        )
      })
    }
    return result
  }

  showSortMenu = () => {
    this.setState({ showMenuSort: true }, () => {
      document.addEventListener("click", this.closeSortMenu);
    });
  };
  closeSortMenu = () => {
    this.setState({ showMenuSort: false }, () => {
      document.removeEventListener("click", this.closeSortMenu);
    });
  };

  showDropdownSort = (sortListByMagic) => {
    const { classes } = this.props;
    let result = null;
    if (sortListByMagic.length > 0) {
      result = sortListByMagic.map((item, index) => {
        let match = item.status ? "activeLanguage" : null
        return (
            <MenuItem className={ clsx(classes.itemMenu, `${match}`)} key={index} classes={{ root: classes.menuItemHover }}>
            {item.label}
          </MenuItem>
        );
      });
    }
    return result;
  }

  render() {
    const { classes } = this.props;
    const {showMenuSort} = this.state;
    return (
      <div className={classes.root}>
          {this.showSort(sortList)}
          <div className={classes.sortContent} onClick={this.showSortMenu}>
          <Typography
          classes={{ root: classes.rootTextSort, }}
          >
            Sort by Magic
            <ArrowDropDown/>
            </Typography>
            {showMenuSort ? 
           <MenuList className={classes.dropdownSort}>
                {this.showDropdownSort(sortListByMagic)}
           </MenuList>
           : null}
          </div>
      </div>
    );
  }
}

export default withStyles(styles)(Sort);

const styles = theme => ({
    root:{
        display: "flex",
        marginRight: "30px"
    },
    textCheckbox:{
        fontSize: "13.8px",
        fontWeight: 600,
        color: "#838789",
        letterSpacing: "-.61px"
    },
    checkbox:{
        color: "#e6e6e6",
        "&:hover":{
          color: "#00aaf4",
          background: "transparent"
        }
      },
      rootTextSort:{
        display: "flex",
        alignItems: "center",
        fontSize: "13.8px",
        fontWeight: 600,
        color: "#838789",
        letterSpacing: "-.61px",
        "&:hover": {
            color: "#20b3e7"
        }
      },
      sortContent:{
          display : "flex",
          alignItems: "center",
          marginLeft: "17px",
          marginBottom: "3px",
          cursor: "pointer",
          position: "relative",

      },
      menuItemHover: {
          color: "#787b83",
          fontWeight: "400",
          lineHeight: "1.66667",
          fontSize: "14.25px",
        "&:hover": {
          backgroundColor: "#e3f3fe"
        }
      },
      dropdownSort: {
        display: "block",
        position: "absolute",
        border: "1px solid rgba(0,0,0,.12)",
        borderRadius: "4px",
        zIndex: "5",
        width: "220px",
        backgroundColor: "white",
        top: "104%",
        left: "-110%",
      },
  });
  
  export default styles;
  
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import { Typography } from '@material-ui/core';
import Sort from "./Sort";

class Title extends Component {

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
          <Typography variant={'h4'} className={classes.title}>
          English Tutors & Teachers  
          </Typography> 
            <Sort/>
      </div>
    );
  }
}

export default withStyles(styles)(Title);

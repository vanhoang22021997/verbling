const styles = theme => ({
    root: {
        width: "83%",
        margin: "0 auto",
        paddingTop: "30px",
        display: "flex",
        justifyContent: "space-between"
    },
    title:{
        fontWeight: 700,
        color: "#4f5457",
        fontSize: "30px"
    }
  });
  
  export default styles;
  
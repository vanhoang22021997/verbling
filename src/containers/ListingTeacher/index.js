/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import Title from "./Title";
import CardsList from "./CardsList";

class ListingTeacher extends Component {

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
          <Title/>
          <CardsList/>
      </div>
    );
  }
}

export default withStyles(styles)(ListingTeacher);

/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import Grid from "@material-ui/core/Grid";
import CardItem from "./CardItem";
import styles from "./styles";
import TeacherDetails from "./TeacherDetails";

const TeachersList = [
  {
    name: "Curtis Davies",
    teaches: "English",
    from: "Canada",
    Lessons: "25",
    price: "464,000 ₫",
    speakes: {
      native: "English",
      B1: "Spanish"
    },
    label:
      "If you want to communicate in English at a more confident level and become a more powerful speaker, I can help you. With over 8 years experience in instructing, both in front of a group of people and one-on-one, online and offline, I can assist you in reaching your English language speaking goals. Business English practice",
    avatar:
      "https://d2tz4rphepbk36.cloudfront.net/verbling-profiles/63701246855924011141_150x150_cropped.png",
    table: {
      Web: ["9:00 AM", "10.00 AM", "13:30 PM"],
      Thu: [],
      Fri: [
        "9:00 AM",
        "10.00 AM",
        "14:30 PM",
        "15:30 PM",
        "16:00 PM",
        "20:30 PM",
        "22:30 PM"
      ],
      Sat: ["10.00 AM", "13:30 PM"],
      video: "https://www.youtube.com/embed/ByT7Q5bYerY?controls=0"
    }
  },
  {
    name: "Lisa Zaski",
    teaches: "English",
    from: "United States",
    Lessons: "1813",
    price: "812,000 ₫",
    speakes: {
      native: "English",
      A2: "Italian"
    },
    label:
      "CALLING NEW STUDENTS! YOU ARE WELCOME TO WRITE TO ME FOR A 50% DISCOUNT COUPON ON OUR FIRST 60-MINUTE TRIAL LESSON. THIS WILL GIVE US THE OPPORTUNITY TO ESTABLISH A CURRICULUM PRECISELY DESIGNED TO MEET YOUR NEEDS! Welcome! My name is Lisa, and I am a TESOL/TEFL/TESL-Certified",
    avatar:
      "https://d2tz4rphepbk36.cloudfront.net/verbling-profiles/75637180346485897727_150x150_cropped.png",
    table: {
      Web: ["9:00 AM", "10.00 AM", "13:30 PM"],
      Thu: [],
      Fri: [],
      Sat: ["10.00 AM", "13:30 PM"],
      video: "https://www.youtube.com/embed/jS_On-gl86A"
    }
  },
  {
    name: "Martha Price",
    teaches: "English",
    from: "United States",
    Lessons: "544",
    price: "951,000 ₫",
    speakes: {
      A1: "Spanish"
    },
    label:
      "I have 30 years of experience teaching English as a Second Language. I have taught all levels; from basic to advanced grammar and conversation, as well as business English, pronunciation, TOEFL and TOEIC prep.  I have taught overseas in the Peace Corps and in the States at language schools, the World Bank, continuing education programs and",
    avatar:
      "https://d2tz4rphepbk36.cloudfront.net/verbling-profiles/34536702578462259735_150x150_cropped.png",
    table: {
      Web: [
        "9:00 AM",
        "10.00 AM",
        "13:30 PM",
        "14:00 PM",
        "14:30 PM",
        "15:30 PM",
        "16:00 PM",
        "20:30 PM"
      ],
      Thu: ["9:00 AM", "10.00 AM", "13:30 PM"],
      Fri: ["9:00 AM", "10.00 AM", "14:30 PM", "15:30"],
      Sat: ["9:00 AM", "10.00 AM", "13:30 PM"],
      video: "https://www.youtube.com/embed/hFAcjBj53a8"
    }
  }
];

class CardsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      check: -1
    };
  }

  toggleHover = index => {
    this.setState({
      check: index
    });
  };

  showCard = TeachersList => {
    const { classes } = this.props;
    const { check } = this.state;
    let result = null;
    if (TeachersList.length > 0) {
      result = TeachersList.map((card, index) => {
        return (
          <>
            <Grid
              item
              lg={8}
              md={8}
              sm={12}
              className={classes.grid}
              key={index}
              onMouseEnter={() => {
                this.toggleHover(index);
              }}
            >
              <CardItem card={card} />
            </Grid>
            <Grid item xs className={classes.detail}>
              <TeacherDetails
                table={card.table}
                selected={check}
                index={index}
              />
            </Grid>
          </>
        );
      });
    }
    return result;
  };

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          {this.showCard(TeachersList)}
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(CardsList);

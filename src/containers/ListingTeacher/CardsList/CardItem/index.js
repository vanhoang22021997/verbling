/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import Avatar from "@material-ui/core/Avatar";
import FavoriteBorder from "@material-ui/icons/FavoriteBorder";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Star from "@material-ui/icons/Star";
import Button from "@material-ui/core/Button";
import styles from "./styles";
import { Grid } from "@material-ui/core";

class CardItem extends Component {

  showSpeakes = speakes => {
    const { classes } = this.props;
    let result = null;
      result = <>
        <span className={classes.language}>{speakes.native}</span>
        <span className={classes.native}>{speakes.native ? 'native' : null}</span>
        <span className={classes.language}>{speakes.A1}</span>
        <span className={classes.CertificateA}>{speakes.A1 ? 'A1' : null}</span>
        <span className={classes.language}>{speakes.A2}</span>
        <span className={classes.CertificateA}>{speakes.A2 ? 'A2' : null}</span>
        <span className={classes.language}>{speakes.B1}</span>
        <span className={classes.CertificateB}>{speakes.B1 ? 'B1' : null}</span>
        <span className={classes.language}>{speakes.B2}</span>
        <span className={classes.CertificateB}>{speakes.B2 ? 'B2' : null}</span>
      </>
    return result
  };

  render() {
    const { classes, card } = this.props;
    return (
      <Card classes={{ root: classes.card }}>
        <div className={classes.avt}>
          <Avatar
            alt="Remy Sharp"
            src={card.avatar}
            classes={{ root: classes.avatar }}
          />
          <div>
            <p className={classes.price}>{card.price}</p>
            <p className={classes.currency}>VND/h</p>
          </div>
          <div className={classes.featured}>Featured</div>
        </div>
        <div className={classes.cardContent}>
          <div className={classes.cardHeader}>
            <div className={classes.headerName}>
              <h2 className={classes.name}>{card.name}</h2>
              <Star className={classes.iconStar} />
              <span className={classes.evaluate}>5.00</span>
            </div>
            <div className={classes.headerBook}>
              <FavoriteBorder classes={{ root: classes.iconHeart }} />
              <Button
                onClick={this.handleClose}
                color="default"
                classes={{
                  text: classes.textBook,
                  root: classes.rootBook
                }}
              >
                Book Free Trial
              </Button>
            </div>
          </div>
          <CardContent>
            <Grid container spacing={2}>
              <Grid container item xs={4} className={classes.titleItem}>
                <div className={classes.titleContent}>
                  <img
                    alt="Flag"
                    src="https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7"
                    className={classes.iconTeaches}
                    language="en"
                    width="20px"
                    height="20px"
                  />
                  <div>
                    <h2 className={classes.titleTop}>Teaches</h2>
                    <p className={classes.titleBot}>{card.teaches}</p>
                  </div>
                </div>
              </Grid>
              <Grid container item xs={4} className={classes.titleItem}>
                <div className={classes.titleContent}>
                  <img
                    alt="Flag"
                    src="https://cdn.verbling.com/static/svg/icons8/2ff29f72b10f9eddac8658589cedd72d.icons8-globe.svg"
                    className={classes.iconTeaches}
                    language="en"
                    width="20px"
                    height="20px"
                  />
                  <div>
                    <h2 className={classes.titleTop}>from</h2>
                    <p className={classes.titleBot}>{card.from}</p>
                  </div>
                </div>
              </Grid>
              <Grid container item xs={4} className={classes.titleItem}>
                <div className={classes.titleContent}>
                  <img
                    alt="Flag"
                    src="https://cdn.verbling.com/static/svg/icons8/8045874736b759666da0cb9c097c7777.icons8-time.svg"
                    className={classes.iconTeaches}
                    language="en"
                    width="20px"
                    height="20px"
                  />
                  <div>
                    <h2 className={classes.titleTop}>Lessons</h2>
                    <p className={classes.titleBot}>{card.Lessons}</p>
                  </div>
                </div>
              </Grid>
            </Grid>
            <p className={classes.textspeakes}>
              Speakes:
              {this.showSpeakes(card.speakes)}
            </p>
            <p className={classes.textContent}>
                {card.label}
            </p>
            <p className={classes.readMore}>Read More</p>
          </CardContent>
        </div>
      </Card>
    );
  }
}

export default withStyles(styles)(CardItem);

const styles = theme => ({
  card: {
    display: "flex",
    padding: "15px",
  },
  cardContent: {
    marginLeft: "15px",
    width: "100%"
  },
  cardHeader: {
    marginTop: "10px",
    display: "flex",
    justifyContent: "space-between",
  },
  avatar: {
    borderRadius: 0,
    width: "150px",
    height: "150px"
  },
  avt:{
    position: "relative",

  },
  featured:{
    position : "absolute",
    top: 0,
    left: 0,
    width: "100%",
    backgroundColor: "#0699cd",
    textAlign: "center",
    fontSize: "12px",
    letterSpacing: "3px",
    zIndex: 2,
    color: "white",
    borderRadius: "4px 4px 0 0"
  },
  price: {
    fontSize: "16px",
    color: "rgb(66, 200, 138)",
    letterSpacing: "1.1px",
    textAlign: "center",
    margin: "10px 0",
    fontWeight: 700
  },
  currency: {
    color: "rgb(120, 123, 131)",
    textAlign: "center",
    margin: 0,
    fontWeight: 700
  },
  headerName: {
    display: "flex",
    color: "rgb(79, 84, 87)",
    fontSize: "14px",
    alignItems: "center"
  },
  name: {
    margin: 0
  },
  iconStar: {
    color: "rgb(255, 189, 4)",
    fontSize: "27px",
    margin: "0 2px"
  },
  evaluate:{
    color: "rgb(120, 123, 131)",
    fontSize: "20px",
    fontWeight: 700
  },
  headerBook:{
    display: "flex",
    alignItems: "center",
  },
  textBook:{
    color: "white",
    background: "#0699cd",
    fontSize: "14.25px",
    border: "2px solid #0699cd",
    fontWeight: "600",
  },
  rootBook: {
    "&:hover": {
      background: "#05749b !important",
      color: "white !important",
    }
  },
  iconHeart:{
    fontSize: "35px",
    opacity: 0.5,
    cursor: "pointer",
    "&:hover":{
        color: "rgb(253, 68, 68)",
        opacity: 1
    }
  },
  titleItem:{
 
    height: "60px",
    padding: "4px !important",
  },
  titleContent:{
    display: "flex",
    alignItems: "center",
    backgroundColor: "rgb(248, 248, 248)",
    width: "100%"
  },
  titleTop:{
      margin: 0,
      color: "rgb(131, 135, 137)",
      fontSize: "12.8px",
      letterSpacing: "-0.61px",
      marginBottom: "2px",
      marginLeft: "5px"
  },
  titleBot:{
      margin: 0,
      color:" rgb(120, 123, 131)",
      fontSize: "14.8px",
      letterSpacing: "-0.61px",
      marginLeft: "5px"
  },
  textSpeakes:{
    color: "rgb(120, 123, 131)",
    fontSize: "14.8px",
    letterSpacing: "-0.61px",
    fontWeight: 700
  },
  language:{
    fontWeight: 300,
    margin: "0 2px"
  },
  native:{
    color: "rgb(6, 153, 205)",
    margin: "0 2px"
  },
  CertificateA:{
    color: "rgb(253, 68, 68)",
    margin: "0 2px"
  },
  CertificateB:{
    color : "rgb(255, 158, 0)",
    margin: "0 2px"
  },
  textContent:{
      color: "#838789",
      fontWeight : 300,
      marginBottom: "5px"
  },
  readMore:{
      color: "#0699cd",
      margin: 0,
      fontWeight : 300
  }
});

export default styles;

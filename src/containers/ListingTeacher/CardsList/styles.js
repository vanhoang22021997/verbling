const styles = theme => ({
  root: {
    width: "83%",
    margin: "0 auto",
    paddingTop: "30px",
    display: "flex",
    justifyContent: "space-between"
  },
  detail: {
    position: "relative"
  }
});

export default styles;

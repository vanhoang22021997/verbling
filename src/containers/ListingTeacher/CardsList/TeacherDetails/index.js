/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Table from "@material-ui/core/Table";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Button from "@material-ui/core/Button";
import { Scrollbars } from "react-custom-scrollbars";

import styles from "./styles";

class TeacherDetails extends Component {
    
  showTable = (daysOfTheWeek) => {
    const {classes} = this.props;
    let result = null;
    if (daysOfTheWeek.length > 0) {
      result = daysOfTheWeek.map((time, index) => {
        return (
            <div className={classes.time} key={index}>{time}</div>
        );
      });
    }
    return result
  };

  render() {
    const { classes, table, selected, index } = this.props;
    return (
      selected === index ? <Card classes={{ root: classes.card }}>
      <div className={classes.root}>
        <iframe
          width="100%"
          height="200px"
          src={table.video}
          frameBorder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowFullScreen
        />
      </div>
      <CardContent>
        <div className={classes.title}>
          <img
            width="30px"
            height="30px"
            alt="circled_left_2"
            src="https://cdn.verbling.com/static/svg/icons8/09c99bdc2bda4773a4041481a5cbb6e1.icons8-circled_left_2.svg"
          />
          <span className={classes.textTitle}>
            All times listed are in your local timezone
          </span>
          <img
            width="30px"
            height="30px"
            alt="circled_right_2"
            src="https://cdn.verbling.com/static/svg/icons8/b38d1bf9492579b2686c58cd1a2f0c1e.icons8-circled_right_2-e.svg"
          />
        </div>
        <div>
          <Table className={classes.table} size="small">
            <TableHead>
              <TableRow>
                <TableCell align="center" classes={{ head: classes.tbHead }}>
                  <span className={classes.titleDayChoose}>Web</span>
                  <span className={classes.dayChoose}>31</span>
                </TableCell>
                <TableCell align="center" classes={{ head: classes.tbHead }}>
                  <span className={classes.titleDay}>Thu</span>
                  <span className={classes.day}>1</span>
                </TableCell>
                <TableCell align="center" classes={{ head: classes.tbHead }}>
                  <span className={classes.titleDay}>Fri</span>
                  <span className={classes.day}>2</span>
                </TableCell>
                <TableCell align="center" classes={{ head: classes.tbHead }}>
                  <span className={classes.titleDay}>Sat</span>
                  <span className={classes.day}>3</span>
                </TableCell>
              </TableRow>
            </TableHead>
          </Table>
          <Scrollbars style={{ height: 130 }}>
          <div className={classes.tb}>
            <div className={classes.colChoose}>
                {this.showTable(table.Web)}
              <div className={classes.time}>1:30 AM</div>
            </div>
            <div className={classes.col}>
                  {this.showTable(table.Thu)}
            </div>
            <div className={classes.col}>
            {this.showTable(table.Fri)}
            </div>
            <div className={classes.col}>
            {this.showTable(table.Sat)}
            </div>
          </div>
          </Scrollbars>
        </div>
        <Button
            onClick={this.handleClose}
            color="default"
            classes={{
              text: classes.textBtnClose,
              root: classes.rootBtnClose
            }}
          >
            View Profile
          </Button>
      </CardContent>
    </Card> : null
    );
  }
}

export default withStyles(styles)(TeacherDetails);

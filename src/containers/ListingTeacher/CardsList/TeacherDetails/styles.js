const styles = theme => ({
  card:{
    position: "absolute",
    top: "10px",
    right: "30px",
    width: "90%",
  },
  title: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center"
  },
  textTitle: {
    color: "#4f5457",
    fontSize: "11.4px"
  },
  tbHead: {
    boxShadow: "rgba(0, 0, 0, 0.1) 0px 3px 2px -2px",
    border: "1px solid rgb(204, 204, 204)",
    backgroundColor: "rgb(248, 248, 248)",
    padding: "0",
    width: "25%"
  },
  titleDayChoose: {
    display: "block",
    color: "rgb(6, 153, 205)",
    fontWeight: "700",
    borderBottom: "1px solid rgb(204, 204, 204)",
    fontSize: "11.4px",
    width: "50%",
    margin: "0 auto"
  },
  titleDay: {
    display: "block",
    color: "#4f5457",
    fontWeight: "700",
    borderBottom: "1px solid rgb(204, 204, 204)",
    fontSize: "11.4px",
    width: "50%",
    margin: "0 auto"
  },
  dayChoose: {
    fontWeight: "300",
    color: "rgb(6, 153, 205)"
  },
  day: {
    fontWeight: "300"
  },
  tb: {
    display: "flex"
  },
  col: {
    width: "25%",
    textAlign: "center"
  },
  colChoose: {
    width: "25%",
    textAlign: "center",
    backgroundColor: "rgb(227, 243, 254)"
  },
  time: {
    color: "rgb(6, 153, 205)",
    fontWeight: "700",
    fontSize: "14.25px",
    padding: "8px"
  },
  textBtnClose: {
    color: "#0699cd",
    fontSize: "14.25px",
    border: "2px solid #0699cd",
    fontWeight: "600",
    width: "100%",
  },
  rootBtnClose: {
      marginTop: "10px",
    "&:hover": {
      background: "#0699cd !important",
      color: "white !important"
    }
  },
  "@media only screen and (max-width: 992px)":{
    "card":{
      display: "none"
    }
}
});

export default styles;

/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Fab from "@material-ui/core/Fab";

import styles from "./styles";
import clsx from "clsx";
import theme from "./../../../commons/theme";
import  './style.css'

const languageList = [
    {
      label: "English",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
      status: true
    },
    {
      label: "French",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1eb-1f1f7.png?v=2.2.7",
      status: false
    },
    {
      label: "German",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1e9-1f1ea.png?v=2.2.7",
      status: false
    },
    {
      label: "Italian",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ee-1f1f9.png?v=2.2.7",
      status: false
    },
    {
      label: "Japanese",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ef-1f1f5.png?v=2.2.7",
      status: false
    },
    {
      label: "Mandarin",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ee-1f1f9.png?v=2.2.7",
      status: false
    },
    {
      label: "Portuguese",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1e9-1f1ea.png?v=2.2.7",
      status: false
    },
    {
      label: "Portuguese",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
      status: false
    },
    {
      label: "Russian",
      img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
      status: false
    }
  ];

class MenuRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenuCurrency: false,
      showMenuLanguage: false
    };
  }
  showCurrency = () => {
    this.setState({ showMenuCurrency: true }, () => {
      document.addEventListener("click", this.closeCurrency);
    });
  };
  closeCurrency = () => {
    this.setState({ showMenuCurrency: false }, () => {
      document.removeEventListener("click", this.closeCurrency);
    });
  };
  showLanguage = () => {
    this.setState({ showMenuLanguage: true }, () => {
      document.addEventListener("click", this.closeLanguage);
    });
  };
  closeLanguage = () => {
    this.setState({ showMenuLanguage: false }, () => {
      document.removeEventListener("click", this.closeLanguage);
    });
  };
  showDropdownLanguage = (languageList) => {
        const { classes } = this.props;
        let result = null;
        if (languageList.length > 0) {
          result = languageList.map((item, index) => {
            let match = item.status ? "activeLanguage" : null
            return (
                <MenuItem className={ clsx(classes.itemMenu, `${match}`)} key={index} classes={{ root: classes.menuItemHover }}>
                <img alt="Flag" src={item.img} width="12px" height="12px" className={classes.img} style={{marginRight:"10px"}}/>
                {item.label}
              </MenuItem>
            );
          });
        }
        return result;
  }

  render() {
    const { classes } = this.props;
    const {showMenuCurrency, showMenuLanguage} = this.state;
    return (
      <Grid item xs={6} container className={classes.grid} justify="flex-end">
        <div className={classes.currency} onClick={this.showCurrency}>
          <a
            className={clsx(classes.menuLinkRight, classes.effHover)}
            style={{ display: "flex", alignItems: "center" }}
          >
            Currency(VND)
            <ArrowDropDown className={classes.iconMenuLeft} />
          </a>
          {showMenuCurrency ? (
            <MenuList className={classes.dropdownRight}>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                AED - United Arab Emirates Dirham
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                AFN - Afghan Afghani
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ALL - Albanian Lek
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                AMD - Armenian Dram
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ANG - Netherlands Antillean Guilder
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                AOA - Angolan Kwanza
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem className={classes.textDropdownRight} classes={{ root: classes.menuItemHover }}>
                ARS - Argentine Peso
              </MenuItem>
            </MenuList>
          ) : null}
        </div>
        <div onClick={this.showLanguage}>
          <a
            className={clsx(classes.menuLinkRight, classes.effHover)}
            style={{ display: "flex", alignItems: "center" }}
          >
            Site Language
            <ArrowDropDown className={classes.iconMenuLeft} />
          </a>
          
          {showMenuLanguage ? 
           <MenuList className={classes.dropdownRightLanguage}>
                {this.showDropdownLanguage(languageList)}
           </MenuList>
           : null}
        </div>
        <a
          className={clsx(classes.menuLinkRight, classes.effHover)}
          style={{ display: "flex", alignItems: "center" }}
        >
          Login
        </a>
        <Fab variant="extended" size="small" className={classes.btnSignup}>
          Signup
        </Fab>
      </Grid>
    );
  }
}

export default withStyles(styles)(MenuRight);

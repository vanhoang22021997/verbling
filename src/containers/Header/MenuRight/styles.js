const styles = theme => ({
  grid: {
    display: "flex",
    alignItems: "center"
  },
  btnSignup: {
    color: "white",
    textTransform: "none",
    backgroundColor: theme.color.secondary,
    boxShadow: "none",
    fontSize: "12px",
    "&:hover": {
      backgroundColor: "white",
      color: theme.color.secondary
    }
  },
  currency: {
    position: "relative"
  },
  dropdownRight: {
    display: "block",
    position: "absolute",
    border: "1px solid rgba(0,0,0,.12)",
    borderRadius: "4px",
    zIndex: "5",
    maxHeight: "350px",
    overflowY: "auto",
    width: "350px",
    backgroundColor: "white"
  },
  textDropdownRight: {
    fontWeight: "400",
    fontSize: "14px"
  },
  menuLinkRight: {
    padding: "10px 10px",
    letterSpacing: "-.68px",
    cursor: "pointer",
    position: "relative",
    height: "40px"
  },
  dropdownRightLanguage: {
    display: "block",
    position: "absolute",
    border: "1px solid rgba(0,0,0,.12)",
    borderRadius: "4px",
    zIndex: "5",
    maxHeight: "350px",
    overflowY: "auto",
    width: "150px",
    backgroundColor: "white",
    paddingTop: "0px"
  },
  menuItemHover: {
    "&:hover": {
      backgroundColor: "#e3f3fe"
    }
  },
});

export default styles;

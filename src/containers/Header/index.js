/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import Toolbar from "@material-ui/core/Toolbar";
import AppBar from "@material-ui/core/AppBar";

import MenuLeft from './MenuLeft'
import MenuRight from './MenuRight'
import ButtonResponsive from './ButtonResponsive'

class Header extends Component {

  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <AppBar position="static" className={classes.header} elevation={0}>
          <Toolbar className={classes.toolbar}>
            <img
              alt="Verbling Logo"
              height="22.87905647408664"
              width="102"
              className="VerblingLogo LandingHeader__logo"
              src="https://cdn.verbling.com/static/svg/6fa1864fd902094a6bb600ebf9368d96.logo.svg"
            />
            <Grid container spacing={2} className={classes.menu}>
                <MenuLeft/>
                <MenuRight/>
            </Grid>
            <ButtonResponsive className={classes.btnResponsive}/>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

export default withStyles(styles)(Header);

const styles = theme => ({
  btnClose:{
    color: "white",
    "&:hover": {
      backgroundColor: "transparent"
    },
    "&:focus": {
      outline: "-webkit-focus-ring-color auto 1px"
    }
  },
  appBar:{
    backgroundImage: "linear-gradient(116deg,#0699cd,#3bb85c)",
    boxShadow: "none",
    border: "none"
  },
  toolbar:{
      display: "flex",
        justifyContent: "space-between"
  },
  bgList:{
    backgroundImage: "linear-gradient(116deg,#0699cd,#3bb85c)",
  },
  bgMenu:{
    backgroundImage: "linear-gradient(116deg,#0699cd,#3bb85c)",
    width: "100%",
    height: "100%"
  },
  listItem:{
    color: "white",
    textAlign: "center",
    transition: "all 1",
    "&:hover":{
        backgroundColor: "transparent",
        transform: "translateY(-1px)"
    }
  },
  listItemSite:{
    color: "white",
    transition: "all 1",
    "&:hover":{
        backgroundColor: "transparent",
        transform: "translateY(-1px)"
    }
  },
  primaryText:{
    fontSize: "20px",
    fontWeight: "700",
    height: "60px"
  },
  divider:{
    height: "2px",
    backgroundColor: "white",
    opacity: "0.5"
  },
});

export default styles;

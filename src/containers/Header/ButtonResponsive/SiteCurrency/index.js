/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import CloseIcon from "@material-ui/icons/Close";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import {
  IconButton,
  Dialog,
  ListItemText,
  ListItem,
  List,
  Divider,
  AppBar,
  Toolbar,
  Slide
} from "@material-ui/core";
import { Scrollbars } from "react-custom-scrollbars";

const listCurrency = [
  "AED - United Arab Emirates Dirham",
  "AFN - Afghan Afghani",
  "AFN - Afghan Afghani",
  "AMD - Armenian Dram",
  "ANG - Netherlands Antillean Guilder",
  "AOA - Angolan Kwanza",
  "ARS - Argentine Peso",
  "AUD - Australian Dollar",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin",
  "AWG - Aruban Florin"
];

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class SiteCurrency extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  showCurrency = listCurrency => {
    const { classes } = this.props;
    let result = null;
    if (listCurrency.length > 0) {
      result = listCurrency.map((item, index) => {
        return (
          <ListItem button classes={{ root: classes.listItem }} key={index}>
            <ListItemText
              classes={{ primary: classes.primaryText }}
              primary={item}
            />
          </ListItem>
        );
      });
    }
    return result;
  };

  render() {
    const { open } = this.state;
    const { classes } = this.props;
    return (
      <List className={classes.bgList}>
        <Divider classes={{ root: classes.divider }} />
        <ListItem
          button
          classes={{ root: classes.listItemSite }}
          onClick={this.handleClickOpen}
        >
          <ListItemText
            classes={{
              root: classes.listText,
              primary: classes.primaryText
            }}
            primary="Site Language"
          />
          <KeyboardArrowRight fontSize={"large"} />
        </ListItem>
        <Dialog
          fullScreen
          open={open}
          onClose={this.handleClose}
          TransitionComponent={Transition}
        >
          <AppBar className={classes.appBar} position={"static"}>
            <Toolbar className={classes.toolbar}>
              <img
                alt="Verbling Logo"
                height="22.87905647408664"
                width="102"
                className="VerblingLogo LandingHeader__logo"
                src="https://cdn.verbling.com/static/svg/7d831ff20c2e7c8f0989fd906ff5d7c5.logo-white.svg"
              />
              <IconButton
                classes={{ root: classes.btnClose }}
                edge="start"
                color="inherit"
                onClick={this.handleClose}
                aria-label="close"
                disableRipple
              >
                <CloseIcon fontSize={"large"} />
              </IconButton>
            </Toolbar>
          </AppBar>
          <div className={classes.bgMenu}>
          <Scrollbars style={{ height: 400 }}>
            <List>
              {this.showCurrency(listCurrency)}
            </List>
          </Scrollbars>
          </div>
        </Dialog>
      </List>
    );
  }
}

export default withStyles(styles)(SiteCurrency);

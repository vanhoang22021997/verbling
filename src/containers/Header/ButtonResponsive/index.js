/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";

import MenuIcon from "@material-ui/icons/Menu";
import CloseIcon from "@material-ui/icons/Close";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import {
  IconButton,
  Dialog,
  ListItemText,
  ListItem,
  List,
  Divider,
  AppBar,
  Toolbar,
  Slide
} from "@material-ui/core";
import { Scrollbars } from "react-custom-scrollbars";
import SiteLanguage from './SiteLanguage'
import SiteCurrency from './SiteCurrency'


const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

class ButtonResponsive extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false
    };
  }
  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({
      open: false
    });
  };

  render() {
    const { open } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <IconButton
          classes={{ root: classes.btn }}
          edge="start"
          className={classes.menuButton}
          color="default"
          aria-label="menu"
          disableRipple
          onClick={this.handleClickOpen}
        >
          <MenuIcon fontSize={"large"}/>
        </IconButton>
        <Dialog
            fullScreen
            open={open}
            onClose={this.handleClose}
            TransitionComponent={Transition}
          >
            <AppBar className={classes.appBar} position={'static'}>
              <Toolbar className={classes.toolbar}>
              <img alt="Verbling Logo" height="22.87905647408664" width="102" className="VerblingLogo LandingHeader__logo" src="https://cdn.verbling.com/static/svg/7d831ff20c2e7c8f0989fd906ff5d7c5.logo-white.svg"/>
                <IconButton
                classes={{ root: classes.btnClose }}
                  edge="start"
                  color="inherit"
                  onClick={this.handleClose}
                  aria-label="close"
                  disableRipple
                >
                  <CloseIcon fontSize={"large"}/>
                </IconButton>
              </Toolbar>
            </AppBar>
            <div className={classes.bgMenu}>
            <Scrollbars style={{ height: 300 }}>
            <List>
              <ListItem button
              classes={{ root: classes.listItemChoose }}
              >
                <ListItemText classes={{primary: classes.primaryText}}
                primary="SIGNUP"/>
              </ListItem>
              
              <ListItem button
              classes={{ root: classes.listItem }}
              >
                <ListItemText classes={{root: classes.listText, primary: classes.primaryText}}
                  primary="Login"
                />
              </ListItem>
              <Divider classes={ {root : classes.divider} } />
              <ListItem button
              classes={{ root: classes.listItem }}
              >
                <ListItemText classes={{root: classes.listText, primary: classes.primaryText}}
                  primary="Find a Teacher"
                />
              </ListItem>
              <ListItem button
              classes={{ root: classes.listItem }}
              >
                <ListItemText classes={{root: classes.listText, primary: classes.primaryText}}
                  primary="Explore Decks"
                />
              </ListItem>
              <ListItem button
              classes={{ root: classes.listItem }}
              >
                <ListItemText classes={{root: classes.listText, primary: classes.primaryText}}
                  primary="Discussions"
                />
              </ListItem>
              <ListItem button
              classes={{ root: classes.listItem }}
              >
                <ListItemText classes={{root: classes.listText, primary: classes.primaryText}}
                  primary="Enterprise"
                />
              </ListItem>
            </List>
            </Scrollbars>
              <SiteLanguage/>
              <SiteCurrency/>
            </div>
          </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(ButtonResponsive);

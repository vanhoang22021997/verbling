const styles = theme => ({
  header: {
    background: "#fff",
    border: "1px solid rgba(0,0,0,.12)"
  },
  menu: {
    color: theme.color.secondary,
    fontWeight: 700
  },
  btnResponsive:{
    display: "none"
  },
  "@media only screen and (max-width: 992px)":{
    "menu":{
      display: "none"
    },
    "btnResponsive":{
      display: "block"
    },
    "toolbar":{
      display: "flex",
      justifyContent: "space-between"
    }
}
});

export default styles;

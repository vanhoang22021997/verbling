/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";

import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import styles from "./styles";
import clsx from "clsx";
import theme from "./../../../commons/theme";

class MenuLeft extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Grid item xs={6} className={classes.grid}>
        <a
          className={clsx(classes.menuLinkLeft, classes.effHover)}
          style={{ display: "flex", alignItems: "center" }}
        >
          Find a Teacher
        </a>
        <a
          className={clsx(classes.menuLinkLeft, classes.effHover)}
          style={{ display: "flex", alignItems: "center" }}
        >
          Enterprise
        </a>
        <div className={classes.community}>
          <a
            className={classes.menuLinkLeft}
            style={{ display: "flex", alignItems: "center" }}
          >
            Community
            <ArrowDropDown className={classes.iconMenuLeft} />
          </a>
          <MenuList className={classes.dropdownLeft}>
            <MenuItem classes={{ root: classes.menuItemHover }}>
              <img
                width="30px"
                height="30px"
                alt="sheets"
                src="https://cdn.verbling.com/static/svg/icons8/a78de82e0257f8b0c1396c0c2b3f690f.icons8-sheets.svg"
              />
              Explore Decks
            </MenuItem>
            <MenuItem classes={{ root: classes.menuItemHover }}>
              <img
                width="30px"
                height="30px"
                alt="collaboration_female_male"
                src="https://cdn.verbling.com/static/svg/icons8/4de3651baca5f9339af3f5ebd5452c72.icons8-collaboration_female_male.svg"
              />
              Discussions
            </MenuItem>
            <MenuItem classes={{ root: classes.menuItemHover }}>
              <img
                width="30px"
                height="30px"
                alt="news"
                src="https://cdn.verbling.com/static/svg/icons8/10a74a8ac4d1cb164b34b9b57f2bd4cd.icons8-news.svg"
              />
              Articles
            </MenuItem>
          </MenuList>
        </div>
      </Grid>
    );
  }
}

export default withStyles(styles)(MenuLeft);

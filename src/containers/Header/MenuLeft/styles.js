const styles = theme => ({
  menuLinkLeft: {
    padding: "10px 15px",
    letterSpacing: "-.68px",
    cursor: "pointer",
    position: "relative",
    height: "40px"
  },
  effHover: {
    "&::before": {
      content: `""`,
      width: "0",
      height: "2px",
      backgroundColor: theme.color.secondary,
      position: "absolute",
      bottom: "-2px",
      left: "0",
      transition: "all .2s ease-in-out 0s",
      transform: " scaleX(0)"
    },
    "&:hover": {
        transform: "translateY(-1px)",
      "&::before": {
        transform: " scaleX(1) translateY(1px)",
        width: "100%"
      }
    }
  },
  grid: {
    display: "flex",
    alignItems: "center"
  },
  community: {
    "&:hover": {
      transform: "translateY(-1px)",
      zIndex: "5",
      "& $dropdownLeft": {
        display: "block",
      },
      "& $iconMenuLeft": {
        transform: "rotate(180deg)"
      }
    }
  },
  dropdownLeft: {
    display: "none",
    position: "absolute",
    width: "165px",
    border: "1px solid rgba(0,0,0,.12)",
    borderRadius: "4px",
    zIndex: "1000",
    backgroundColor: "white"
  },
  menuItemHover:{
    "&:hover":{
      backgroundColor: "#e3f3fe"
    }
  },
  iconMenuLeft: {
    transition: "all 0.2s"
  },
});

export default styles;

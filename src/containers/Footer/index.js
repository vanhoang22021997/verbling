/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import Grid from "@material-ui/core/Grid";
import FooterItem from "./FooterItem";
import Social from "./Social";

const listItem = [
  {
    name: "Verbling",
    content: [
      "Apply to Teach",
      "Mobile Apps",
      "Explore Decks",
      "Articles",
      "Discussions",
      "About",
      "Support",
      "The Verbling Language Game",
      "Privacy"
    ]
  },
  {
    name: "Teachers",
    content: [
      "English Tutors",
      "Spanish Tutors",
      "French Tutors",
      "Japanese Tutors",
      "All Tutors",
      "Skype Tutors"
    ]
  },
  {
    name: "Languages",
    content: [
      "Learn English",
      "Learn Chinese",
      "Learn Spanish",
      "Learn French",
      "Learn Japanese",
      "Other Languages"
    ]
  }
];

class Footer extends Component {
  showFooterItem = listItem => {
    const { classes } = this.props;
    let result = null;
    if (listItem.length > 0) {
      result = listItem.map((item, index) => {
        return (
          <Grid item lg={3} md={3} sm={6} className={classes.grid} key={index}>
            <FooterItem item={item} />
          </Grid>
        );
      });
    }
    return result;
  };
  render() {
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={3}>
          {this.showFooterItem(listItem)}
          <Grid item lg={3} md={3} sm={6} xs={3}>
            <Social />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(Footer);

const styles = theme => ({
  title:{
    color : "#4f5457",
    fontSize : "18px",
    margin: "5px"
  },
  text:{
    color: "white",
    fontWeight: 700,
    fontSize: "14.25px",
    cursor: "pointer",
    transition: "color 0.1s",
    margin: "5px",
    "&:hover":{
      color: "#0699cd"
    }
  }
  });
  
  export default styles;
  
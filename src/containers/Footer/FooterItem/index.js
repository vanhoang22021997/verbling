/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import Grid from "@material-ui/core/Grid";

class FooterItem extends Component {
  showText = texts => {
    const { classes } = this.props;
    let result = null;
    if (texts.length > 0) {
      result = texts.map((text, index) => {
        return (
          <p className={classes.text} key={index}>
            {text}
          </p>
        );
      });
    }
    return result;
  };

  render() {
    const { classes, item } = this.props;
    return (
      <div className={classes.root}>
        <h3 className={classes.title}>{item.name}</h3>
        {this.showText(item.content)}
      </div>
    );
  }
}

export default withStyles(styles)(FooterItem);

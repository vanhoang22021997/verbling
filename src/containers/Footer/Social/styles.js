const styles = theme => ({
  title: {
    color: "#4f5457",
    fontSize: "18px",
    margin: "5px"
  },
  text: {
    color: "white",
    fontWeight: 700,
    fontSize: "14.25px",
    cursor: "pointer",
    transition: "color 0.1s",
    margin: "5px",
    display: "flex",
    alignItems: "center",
    "&:hover": {
      color: "#0699cd"
    }
  },
  textBtn: {
    color: "white",
    fontSize: "14.25px",

    fontWeight: "600"
  },
  rootBtn: {
    background: "transparent",
    boxShadow: "none",
    border: "1px solid",
    width: "150px",
    height: "50px",
    borderRadius: "8px",
    "&:hover": {
      background: "transparent",
      color: "white !important"
    },
    "&:focus": {
      background: "#045a79"
    }
  },
  currency:{
    position: "relative"
  },
  dropdownRight: {
    display: "block",
    position: "absolute",
    border: "1px solid rgba(0,0,0,.12)",
    borderRadius: "4px",
    zIndex: "5",
    maxHeight: "350px",
    overflowY: "auto",
    width: "350px",
    backgroundColor: "white",
    bottom: "55px",
    right: "155px"
  },
  textDropdownRight: {
    fontWeight: "400",
    fontSize: "14px",
    color: "#787b83",
  },
  menuLinkRight: {
    padding: "10px 10px",
    letterSpacing: "-.68px",
    cursor: "pointer",
    position: "relative",
    height: "40px"
  },
  language:{
    position: "relative",
  },
  dropdownRightLanguage: {
    display: "block",
    position: "absolute",
    border: "1px solid rgba(0,0,0,.12)",
    borderRadius: "4px",
    zIndex: "5",
    maxHeight: "350px",
    overflowY: "auto",
    width: "150px",
    backgroundColor: "white",
    paddingTop: "0px",
    bottom: "55px",
    left: "0",
  },
  menuItemHover: {
    "&:hover": {
      backgroundColor: "#e3f3fe"
    }
  },
  copyRight:{
      color: "#4f5457",
      fontSize: "11.4px",
  }
});

export default styles;

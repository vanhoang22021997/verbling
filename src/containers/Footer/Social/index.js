/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import clsx from "clsx";
import styles from "./styles";

const languageList = [
  {
    label: "English",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
    status: true
  },
  {
    label: "French",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1eb-1f1f7.png?v=2.2.7",
    status: false
  },
  {
    label: "German",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1e9-1f1ea.png?v=2.2.7",
    status: false
  },
  {
    label: "Italian",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ee-1f1f9.png?v=2.2.7",
    status: false
  },
  {
    label: "Japanese",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ef-1f1f5.png?v=2.2.7",
    status: false
  },
  {
    label: "Mandarin",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ee-1f1f9.png?v=2.2.7",
    status: false
  },
  {
    label: "Portuguese",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1e9-1f1ea.png?v=2.2.7",
    status: false
  },
  {
    label: "Portuguese",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
    status: false
  },
  {
    label: "Russian",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
    status: false
  }
];

class Social extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenuCurrency: false,
      showMenuLanguage: false
    };
  }
  showCurrency = () => {
    this.setState({ showMenuCurrency: true }, () => {
      document.addEventListener("click", this.closeCurrency);
    });
  };
  closeCurrency = () => {
    this.setState({ showMenuCurrency: false }, () => {
      document.removeEventListener("click", this.closeCurrency);
    });
  };
  showLanguage = () => {
    this.setState({ showMenuLanguage: true }, () => {
      document.addEventListener("click", this.closeLanguage);
    });
  };
  closeLanguage = () => {
    this.setState({ showMenuLanguage: false }, () => {
      document.removeEventListener("click", this.closeLanguage);
    });
  };

  showDropdownLanguage = languageList => {
    const { classes } = this.props;
    let result = null;
    if (languageList.length > 0) {
      result = languageList.map((item, index) => {
        let match = item.status ? "activeLanguage" : null;
        return (
          <MenuItem
            className={clsx(classes.itemMenu, `${match}`)}
            key={index}
            classes={{ root: classes.menuItemHover }}
          >
            <img
              alt="Flag"
              src={item.img}
              width="12px"
              height="12px"
              className={classes.img}
              style={{ marginRight: "10px" }}
            />
            {item.label}
          </MenuItem>
        );
      });
    }
    return result;
  };

  render() {
    const { showMenuCurrency, showMenuLanguage } = this.state;
    const { classes } = this.props;
    return (
      <div className={classes.root}>
        <h3 className={classes.title}>Social</h3>
        <p className={classes.text}>
          <img
            width="30px"
            height="30px"
            alt="facebook"
            src="https://cdn.verbling.com/static/svg/icons8/5760a171ebc6096f738e2431752ca019.icons8-facebook.svg"
            style={{ marginRight: "5px" }}
          />
          Facebook (110.3k)
        </p>
        <p className={classes.text}>
          <img
            width="30px"
            height="30px"
            alt="facebook"
            src="https://cdn.verbling.com/static/svg/icons8/3de4c9fb53165f652df40ab54a995bd2.icons8-twitter.svg"
            style={{ marginRight: "5px" }}
          />
          Twitter (2.3k)
        </p>
        <p className={classes.text}>
          <img
            width="30px"
            height="30px"
            alt="facebook"
            src="https://cdn.verbling.com/static/svg/icons8/45abcea91a0432902e98c5db1ea83df0.icons8-instagram_new.svg"
            style={{ marginRight: "5px" }}
          />
          Instagram (6.1k)
        </p>
        <div className={classes.currency} onClick={this.showCurrency}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            disableFocusRipple={true}
            disableRipple
            classes={{
              text: classes.textBtn,
              root: classes.rootBtn
            }}
          >
            ₫ - VND
            <ArrowDropDown
              className={classes.rightIcon}
              style={{ marginLeft: "40px" }}
            >
              ₫ - VND
            </ArrowDropDown>
          </Button>
          {showMenuCurrency ? (
            <MenuList className={classes.dropdownRight}>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                AED - United Arab Emirates Dirham
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                AFN - Afghan Afghani
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ALL - Albanian Lek
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                AMD - Armenian Dram
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ANG - Netherlands Antillean Guilder
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                AOA - Angolan Kwanza
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ARS - Argentine Peso
              </MenuItem>
              <MenuItem
                className={classes.textDropdownRight}
                classes={{ root: classes.menuItemHover }}
              >
                ARS - Argentine Peso
              </MenuItem>
            </MenuList>
          ) : null}
        </div>
        <div className={classes.language} onClick={this.showLanguage}>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            disableFocusRipple={true}
            disableRipple
            style={{ marginTop: "10px" }}
            classes={{
              text: classes.textBtn,
              root: classes.rootBtn
            }}
          >
            English
            <ArrowDropDown
              className={classes.rightIcon}
              style={{ marginLeft: "40px" }}
            >
              English
            </ArrowDropDown>
          </Button>
          {showMenuLanguage ? (
            <MenuList className={classes.dropdownRightLanguage}>
              {this.showDropdownLanguage(languageList)}
            </MenuList>
          ) : null}
        </div>
       <div style={{ marginTop: "10px" }}><small className={classes.copyRight}>© 2019 Verbling</small></div>
      </div>
    );
  }
}

export default withStyles(styles)(Social);

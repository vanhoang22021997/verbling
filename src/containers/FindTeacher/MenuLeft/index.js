/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import SearchTeacher from './SearchTeacher'
import Calendar from './Calendar'

class MenuLeft extends Component {
  render() {
    const { classes } = this.props;

    return (
      <Grid item xs={6} className={classes.grid}>
        <SearchTeacher />
        <Calendar/>
      </Grid>
    );
  }
}

export default withStyles(styles)(MenuLeft);

import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import Check from "@material-ui/icons/Check";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";

import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Typography from "@material-ui/core/Typography";
import { Scrollbars } from "react-custom-scrollbars";
import styles from "./styles";
import theme from "./../../../../commons/theme";


const filterList = [
  {
    label: "English",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
    status: true
  },
  {
    label: "French",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1eb-1f1f7.png?v=2.2.7",
    status: false
  },
  {
    label: "German",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1e9-1f1ea.png?v=2.2.7",
    status: false
  },
  {
    label: "Italian",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ee-1f1f9.png?v=2.2.7",
    status: false
  },
  {
    label: "Japanese",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ef-1f1f5.png?v=2.2.7",
    status: false
  },
  {
    label: "Mandarin",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1ee-1f1f9.png?v=2.2.7",
    status: false
  },
  {
    label: "Portuguese",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1e9-1f1ea.png?v=2.2.7",
    status: false
  },
  {
    label: "Portuguese",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
    status: false
  },
  {
    label: "Russian",
    img: "https://cdn.jsdelivr.net/emojione/assets/png/1f1fa-1f1f8.png?v=2.2.7",
    status: false
  }
];

class SearchTeacher extends Component {
    constructor(props) {
        super(props);
        this.state = {
          showSearchField: false
        };
      }
  showMenu = () => {

    this.setState({ showSearchField: true }, () => {
      document.addEventListener("click", this.closeMenu);
    });
  };
  closeMenu = () => {
    this.setState({ showSearchField: false }, () => {
      document.removeEventListener("click", this.closeMenu);
    });
  };
  showFilterList = filterList => {
    const { classes } = this.props;
    let result = null;
    if (filterList.length > 0) {
      result = filterList.map((item, index) => {
        return (
          <MenuItem className={classes.itemMenu} key={index}>
            {item.status === true ? (
              <Check className={classes.iconSearch} />
            ) : null}
            <img alt="Flag" src={item.img} width="12px" height="12px" className={classes.img}/>
            {item.label}
          </MenuItem>
        );
      });
    }
    return result;
  };
  render() {
    const { classes } = this.props;
    const { showSearchField } = this.state;
    return (
      <div className={classes.inputFindTeacher}>
        <FormControl className={classes.formControl} onClick={this.showMenu}>
          <InputLabel htmlFor="findTeacher" className={classes.labelInput}>
            Teacher
          </InputLabel>
          <Input
            id="findTeacher"
            placeholder="English"
            className={classes.input}
            classes={{
              input: classes.input,
              underline: classes.inputUnderline,
              focused: classes.focused
            }}
            inputProps={{
              "aria-label": "description"
            }}
            startAdornment={
              <InputAdornment position="start">
                <img
                  width="24px"
                  height="24px"
                  alt="language"
                  src="https://cdn.verbling.com/static/svg/icons8/8f36daab5dc454758e7db272aad83348.icons8-language.svg"
                  className={classes.iconInput}
                />
              </InputAdornment>
            }
            endAdornment={
              <InputAdornment position="end">
                {showSearchField === true ? <ArrowDropUp /> : <ArrowDropDown />}
              </InputAdornment>
            }
          />
          {showSearchField === true ? (
            <MenuList className={classes.dropdownSearch}>
              <Scrollbars style={{ height: 350 }}>
                <MenuItem className={classes.itemMenu}>Any Language</MenuItem>
                <Typography
                  variant="h6"
                  component="h2"
                  className={classes.textPopular}
                >
                  Popular
                </Typography>
                {this.showFilterList(filterList)}
              </Scrollbars>
            </MenuList>
          ) : null}
        </FormControl>
      </div>
    );
  }
}

export default withStyles(styles)(SearchTeacher);

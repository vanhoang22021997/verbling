const styles = theme => ({
    inputFindTeacher: {
      padding: "5px",
      width: "230px",
      height: "50px",
      display: "flex",
      border: "1px solid rgba(0,0,0,.12)",
      borderRadius: "10px 0 0 10px"
    },
    labelInput: {
      fontSize: "17px",
      color: "#838789",
      letterSpacing: "-.61px",
      fontWeight: "700",
      left: "31px",
      top: "8px"
    },
    inputUnderline: {
      cursor : "pointer",
      "&::before" : {
          content : "none"
      },
      "&::after" : {
          border: "none",
          backgroundImage: "linear-gradient(116deg,#0699cd,#3bb85c)",
          height: "2px",
          width: "0",
          transition: "width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms",
          transform: "none",
      },
    },
    focused: {
      "&::after":{
          width: "100%"
      }
    },
    iconInput: {
      marginBottom: "15px",
      fontSize: "20px",
      color: theme.color.secondary
    },
    iconSearch:{
      fontSize: "20px",
      color: theme.color.secondary
    },
    input: {
      "&::-webkit-input-placeholder": {
        fontSize: "14px",
        color: "#4f5457"
      }
    },
    textField: {
      width: "100%"
    },
    dropdownSearch: {
      position: "absolute",
      maxHeight: "350px",
      overflowY: "auto",
      top: "55px",
      left: "-16px",
      width: "250px",
      border: "1px solid rgba(0,0,0,.12)",
      padding: "0",
      borderRadius: "0 0 10px 10px ",
      boxShadow: "0px 9px 3px -4px rgba(214,214,214,0.86);",
      zIndex: 5,
      backgroundColor: "white"
    },
    itemMenu: {
      fontSize: "14.25px",
      lineHeight: "1.66667",
      color: "#4f5457",
      padding: "0 14px",
      minHeight: "40px",
      display: "flex",
      alignItems: "center"
    },
    textPopular:{
      fontSize: "14.25px",
      lineHeight: "1.66667",
      color: "#4f5457",
      marginLeft: "16px"
    },
    img:{
      marginRight: "10px"
    }
  });
  
  export default styles;
  
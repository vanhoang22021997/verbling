const styles = (theme) => ({
    inputCalendar: {
      padding: "5px",
      width: "230px",
      height: "50px",
      display: "flex",
      border: "1px solid rgba(0,0,0,.12)",
      borderRadius: "0 10px 10px 0",
    },
    labelInput: {
      fontSize: "17px",
      color: "#838789",
      letterSpacing: "-.61px",
      fontWeight: "700",
      left: "31px",
      top: "8px"
    },
    inputUnderline: {
      cursor : "pointer",
      "&::before" : {
          content : "none"
      },
      "&::after" : {
          border: "none",
          backgroundImage: "linear-gradient(116deg,#0699cd,#3bb85c)",
          height: "2px",
          width: "0",
          transition: "width 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms",
          transform: "none",
      },
    },
    focused: {
      "&::after":{
          width: "100%"
      }
    },
    iconInput: {
      marginBottom: "15px",
      fontSize: "20px",
      color: theme.color.secondary
    },
    input: {
      "&::-webkit-input-placeholder": {
        fontSize: "14px",
        color: "#4f5457"
      }
    },
    button: {
        display: "block",
        backgroundColor: "white"
    },
    dropdownCalendar: {
      position: "absolute",
      top: "55px",
      left: -100,
      width: "500px",
      border: "1px solid rgba(0,0,0,.12)",
      padding: "0",
      borderRadius: "0 0 10px 10px ",
      boxShadow: "0px 9px 3px -4px rgba(214,214,214,0.86);",
      backgroundColor : "white",
      zIndex: "5"
    },
    textButton:{
      fontSize: "13.25px",
      lineHeight: "1.66667",
      color: "#333",
      fontWeight: "600"
    },
    textCalendarTitle:{
      fontSize: "14.25px",
      lineHeight: "1.66667",
      color: "#4f5457",
      margin: "16px 0 16px 16px",
    },
    menuCalendarTop:{
      display: "flex",
      marginLeft: "16px",
    },
    hoverButton: {
        boxShadow: "none",
        border: "1px solid #ccc",
        "&:hover":{
            boxShadow: "0 2px 2px 0 rgba(0,0,0,.1)",
            backgroundColor: "#e3f3fe"
        }
    },
    menuCalendarbottom:{
      display: "flex",
      marginLeft: "16px",
      marginBottom: "30px"
    },
    "@media only screen and (max-width: 992px)":{
      "inputCalendar":{
        display: "none"
      }
  }
  });
  
  export default styles;
  
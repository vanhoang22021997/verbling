import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";
import Input from "@material-ui/core/Input";
import InputLabel from "@material-ui/core/InputLabel";
import InputAdornment from "@material-ui/core/InputAdornment";
import FormControl from "@material-ui/core/FormControl";
import MenuList from "@material-ui/core/MenuList";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";

import styles from "./styles";
import theme from "./../../../../commons/theme";

const calendarList = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

class Calendar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenuCalendar: false
    };
  }
  showCalendar = calendarList => {
    const { classes } = this.props;
    let result = null;
    if (calendarList.length > 0) {
      result = calendarList.map((item, index) => {
        return (
          <Button
            variant="contained"
            className={classes.button}
            classes={{ contained: classes.hoverButton }}
            key={index}
          >
            <Typography
              variant="h6"
              component="h2"
              className={classes.textButton}
            >
              {item}
            </Typography>
          </Button>
        );
      });
    }
    return result;
  };
  showMenu = () => {
    this.setState({ showMenuCalendar: true }, () => {
      document.addEventListener("click", this.closeMenu);
    });
  };
  closeMenu = () => {
    this.setState({ showMenuCalendar: false }, () => {
      document.removeEventListener("click", this.closeMenu);
    });
  };
  render() {
    const { classes } = this.props;
    const { showMenuCalendar } = this.state;
    return (
      <div className={classes.inputCalendar}>
        <FormControl className={classes.formControl} onClick={this.showMenu}>
          <InputLabel htmlFor="calendar" className={classes.labelInput}>
            Availability
          </InputLabel>
          <Input
            id="calendar"
            placeholder="Select Times"
            classes={{
              input: classes.input,
              underline: classes.inputUnderline,
              focused: classes.focused
            }}
            inputProps={{
              "aria-label": "description"
            }}
            startAdornment={
              <InputAdornment position="start">
                <img
                  width="24px"
                  height="24px"
                  alt="calendar"
                  src="https://cdn.verbling.com/static/svg/icons8/8110dc645330172da02b881d21d8ee28.icons8-calendar.svg"
                  className={classes.iconInput}
                />
              </InputAdornment>
            }
            endAdornment={
              <InputAdornment position="end">
                 {showMenuCalendar === true ? <ArrowDropUp /> : <ArrowDropDown />}
              </InputAdornment>
            }
          />
            {showMenuCalendar === true ? (  <MenuList className={classes.dropdownCalendar}>
            <Typography
              variant="h6"
              component="h2"
              className={classes.textCalendarTitle}
            >
              Time of Day
            </Typography>
            <div className={classes.menuCalendarTop}>
              <Button
                variant="contained"
                className={classes.button}
                classes={{ contained: classes.hoverButton }}
              >
                <img
                  width="30px"
                  height="30px"
                  alt="partly_cloudy_day"
                  src="https://cdn.verbling.com/static/svg/icons8/48282dbccf7cd484dbeb475e9da33181.icons8-partly_cloudy_day.svg"
                  className={classes.iconCalendar}
                />
                <Typography
                  variant="h6"
                  component="h2"
                  className={classes.textButton}
                >
                  Early Morning
                </Typography>
              </Button>
              <Button
                variant="contained"
                className={classes.button}
                classes={{ contained: classes.hoverButton }}
              >
                <img
                  width="30px"
                  height="30px"
                  alt="sunrise"
                  src="https://cdn.verbling.com/static/svg/icons8/470c5669aa5ba82f6963c4ae5be21381.icons8-sunrise.svg"
                />
                <Typography
                  variant="h6"
                  component="h2"
                  className={classes.textButton}
                >
                  Morning
                </Typography>
              </Button>
              <Button
                variant="contained"
                className={classes.button}
                classes={{ contained: classes.hoverButton }}
              >
                <img
                  width="30px"
                  height="30px"
                  alt="sun"
                  src="https://cdn.verbling.com/static/svg/icons8/26750580be019886e410a7c7cc727d9f.icons8-sun.svg"
                />
                <Typography
                  variant="h6"
                  component="h2"
                  className={classes.textButton}
                >
                  Afternoon
                </Typography>
              </Button>
              <Button
                variant="contained"
                className={classes.button}
                classes={{ contained: classes.hoverButton }}
              >
                <img
                  width="30px"
                  height="30px"
                  alt="sun"
                  src="https://cdn.verbling.com/static/svg/icons8/aa53bcb429c5771cc8e11186fc853d37.icons8-new_moon.svg"
                />
                <Typography
                  variant="h6"
                  component="h2"
                  className={classes.textButton}
                >
                  Evening
                </Typography>
              </Button>
            </div>
            <Typography
              variant="h6"
              component="h2"
              className={classes.textCalendarTitle}
            >
              Day of the Week
            </Typography>

            <div className={classes.menuCalendarbottom}>
              {this.showCalendar(calendarList)}
            </div>
          </MenuList>) : (null)}
        </FormControl>
      </div>
    );
  }
}

export default withStyles(styles)(Calendar);

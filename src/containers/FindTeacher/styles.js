const styles = () => ({
  findTeacher: {
    height: "75px",
    border: "1px solid rgba(0,0,0,.12)",
    backgroundColor: "white",
    zIndex: 6,
  },
  grid: {
    display: "flex",
  },
  container:{
    width: "83%",
    margin: "0 auto"
  },
  fixed:{
    position: "fixed",
    top: 0,
    width: "100%",
  }
});

export default styles;

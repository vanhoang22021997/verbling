const styles = () => ({
    dropdownSearch: {
      position: "absolute",
      maxHeight: "400px",
      overflowY: "auto",
      top: "68px",
      left: "-125px",
      width: "300px",
      border: "1px solid rgba(0,0,0,.12)",
      padding: "0",
      borderRadius: "0 0 10px 10px ",
      boxShadow: "0px 9px 3px -4px rgba(214,214,214,0.86);",
      zIndex: 5,
      backgroundColor: "white"
    },
    itemMenu: {
      fontSize: "14.25px",
      lineHeight: "1.66667",
      color: "#4f5457",
      padding: "0 0",
      minHeight: "40px",
      "&:hover":{
        backgroundColor: "#e3f3fe"
      }
    },
    textSearch: {
      fontSize: "14.25px",
      lineHeight: "1.66667",
      color: "#4f5457",
      marginLeft: "14px"
    },
    inputSearch: {
      display: "flex",
      alignItems: "center",
      height: "37px",
      borderBottom: "1px solid #d8d8d8",
    },
    input: {
      padding: "5px",
      border: "none",
      fontSize: "14px",
      letterSpacing: "-.028rem",
      fontWeight: "400",
      "::placeholder": {
        color: "#4f5457"
      },
      "&:focus":{
        outline: "none"
      }
    },
    iconSearch:{
      marginLeft: "10px",
      color: "#838789",
      fontSize: "20px"
    },
    checkbox:{
      color: "#e6e6e6",
      "&:hover":{
        color: "#00aaf4",
      }
    }
  });
  
  export default styles;
  
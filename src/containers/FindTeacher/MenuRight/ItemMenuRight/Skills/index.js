/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import SearchIcon from "@material-ui/icons/Search";

import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";

import { Scrollbars } from "react-custom-scrollbars";
import styles from "./styles";

const skillsList = ["ACT", "AP", "APTIS", "BEC", "CAE", "CPE", "ESOL", "FCE", "GMAT", "GRE", "ICAS", "IELTS", "IGCSE"]


class Skills extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: -1
    };
  }

  handleChange = index => event => {
    this.setState({
      checked : index
    });
  };

  showList = (skillsList)=>{
    const { classes } = this.props
    const { checked } = this.state;
    let result = null
    if(skillsList.length > 0){
      result = skillsList.map ( (item,index) => {
        return (
        <MenuItem className={classes.itemMenu} key={index}>
          <Checkbox
            checked={checked === index}
            onChange={this.handleChange(index)}
            value="checked"
            color="primary"
            inputProps={{
              "aria-label": "secondary checkbox"
            }}
            classes={{ root: classes.checkbox }}
          />
          {item}
        </MenuItem>
        )
      })
    }
    return result
  }

  render() {
    const { classes } = this.props;
    return (
      <MenuList className={classes.dropdownSearch}>
        <Scrollbars style={{ height: 400 }}>
          <div className={classes.inputSearch}>
            <SearchIcon className={classes.iconSearch} />
            <input
              type="search"
              name="q"
              placeholder="Search..."
              className={classes.input}
            />
          </div>
          <Typography
            variant="h6"
            component="h2"
            className={classes.textSearch}
          >
            Test preparation
          </Typography>
            {this.showList(skillsList)}
        </Scrollbars>
      </MenuList>
    );
  }
}

export default withStyles(styles)(Skills);

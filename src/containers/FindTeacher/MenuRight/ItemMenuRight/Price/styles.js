const styles = () => ({
  dropdownSearch: {
    position: "absolute",
    height: "190px",
    overflowY: "auto",
    top: "68px",
    left: "-95px",
    width: "390px",
    border: "1px solid rgba(0,0,0,.12)",
    paddingTop: " 10px !important",
    borderRadius: "0 0 10px 10px ",
    boxShadow: "0px 9px 3px -4px rgba(214,214,214,0.86);",
    zIndex: 5,
    backgroundColor: "white"
  },
  number: {
    marginLeft: "10px",
    color: "#838789",
    fontWeight: "700"
  },
  slider: {
    width: "80%",
    margin: "0 auto"
  },
  imgChart:{
    marginLeft: "10px"
  },
  inputRoot:{
    color: "#838789",
    fontSize: "17px !important",
    fontWeight: "700",
    position: "relative",
    width: "88px"
  },
  textTop:{
    position: "absolute",
    left: "19%",
    top: "13px",
    fontSize: "17px"
  },
  textBottom:{
    position: "absolute",
    left: "43%",
    top: "13px",
    fontSize: "17px"
  },
  textAverage:{
    fontWeight: "700",
    color: "#4f5457",
    fontSize: "11px",
    position: "absolute",
    left: "72%",
    top: "7%",
  },
  root:{
    color: "#3bb85c",
    height: "5px"
  },
  thumb:{
    backgroundColor: "white",
    border: "1px solid #e6e6e6",
    boxShadow: "0 2px 2px rgba(72,72,72,.3)",
    width: "30px",
    height: "30px",
    top: "5%"
  }
});

export default styles;

/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import Slider from "@material-ui/core/Slider";
import Input from "@material-ui/core/Input";

import MenuList from "@material-ui/core/MenuList";


import styles from "./styles";

class From extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: [117000 , 1750000]
    };
  }

  handleSliderChange = (event, newValue) => {
    this.setState({
      value: newValue
    });
  };

  handleInputChange = event => {
    this.setState({
      value: event.target.value === "" ? "" : Number(event.target.value)
    });
  };

  handleBlur = () => {
    const { value } = this.state;
    if (value < 117000) {
      this.setState({
        value: 117000
      });
    } else if (value > 1750000) {
      this.setState({
        value: 1750000
      });
    }
  };

  handleChange = name => event => {
    this.setState({
      [name]: event.target.checked
    });
  };

  render() {
    const { classes } = this.props;
    const { value } = this.state;
    return (
      <MenuList
        className={classes.dropdownSearch}
        id="priceSlider"
        style={{ paddingTop: "50px" }}
      >
        <div className={classes.root}>
          <div className={classes.number}>
            <Input
              value={value[0]}
              classes = {{ root: classes.inputRoot, input:classes.input }}
              margin="dense"
              onChange={this.handleInputChange}
              onBlur={this.handleBlur}
              disabled= {true}
              disableUnderline= {true}
              inputProps={{
                step: 10,
                min: 117000,
                max: 1750000,
                type: "number",
                "aria-labelledby": "range-slider"
              }}
            />
            <span className={classes.textTop}>₫-</span>
            <Input
              classes = {{ root: classes.inputRoot, input:classes.input }}
              value={value[1]}
              margin="dense"
              onChange={this.handleInputChange}
              onBlur={this.handleBlur}
              disabled= {true}
              disableUnderline= {true}
              inputProps={{
                step: 10,
                min: 117000,
                max: 1750000,
                type: "number",
                "aria-labelledby": "range-slider"
              }}
            />
            <span className={classes.textBottom}>₫</span>
            <span className={classes.textAverage}>Average: 488,000 ₫</span>
          </div>
            <img  src="/img/chartprice.jpg" alt="chart" className={classes.imgChart}/>
          <div className={classes.slider}>
            <Slider
              classes = {{ root: classes.root, thumb:classes.thumb}}
              value={value}
              onChange={this.handleSliderChange}
              aria-labelledby="range-slider"
              min={117000}
              max={1750000}
            />
          </div>
        </div>
      </MenuList>
    );
  }
}

export default withStyles(styles)(From);

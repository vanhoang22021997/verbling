/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import Checkbox from "@material-ui/core/Checkbox";

import styles from "./styles";

const genderList = ["Female", "Male"]


class Gender extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: -1
    };
  }

  handleChange = index => event => {
    this.setState({
      checked: index
    });
  };

  showList = (genderList)=>{
    const { classes } = this.props
    const { checked } = this.state;
    let result = null
    if(genderList.length > 0){
      result = genderList.map ( (item,index) => {
        return (
        <MenuItem className={classes.itemMenu} key={index}>
          <Checkbox
            checked={checked === index}
            onChange={this.handleChange(index)}
            value="checked"
            color="primary"
            inputProps={{
              "aria-label": "secondary checkbox"
            }}
            classes={{ root: classes.checkbox }}
          />
          {item}
        </MenuItem>
        )
      })
    }
    return result
  }

  render() {
    const { classes } = this.props;
    return (
      <MenuList className={classes.dropdownSearch}>
            {this.showList(genderList)}
      </MenuList>
    );
  }
}

export default withStyles(styles)(Gender);

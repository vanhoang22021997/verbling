const styles = () => ({
    dropdownSearch: {
      position: "absolute",
      maxHeight: "400px",
      overflowY: "auto",
      top: "68px",
      left: "-85px",
      width: "240px",
      border: "1px solid rgba(0,0,0,.12)",
      padding: "0",
      borderRadius: "0 0 10px 10px ",
      boxShadow: "0px 9px 3px -4px rgba(214,214,214,0.86);",
      zIndex: 5,
      backgroundColor: "white"
    },
    itemMenu: {
      fontSize: "14.25px",
      lineHeight: "1.66667",
      color: "#4f5457",
      padding: "0 0",
      minHeight: "40px",
      "&:hover":{
        backgroundColor: "#e3f3fe"
      }
    },
    checkbox:{
      color: "#e6e6e6",
      backgroundColor: "white !important",
      "&:hover":{
        color: "#00aaf4",
        backgroundColor: "transparent !important"
      }
    }
  });
  
  export default styles;
  
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import SearchIcon from "@material-ui/icons/Search";

import Typography from "@material-ui/core/Typography";
import Checkbox from "@material-ui/core/Checkbox";

import { Scrollbars } from "react-custom-scrollbars";
import styles from "./styles";

const TeacherSpeakesList = ["English", "Spanish", "French", "German", "Italian", "Portuguese", "Janpanese", "Russian"]


class TeacherSpeakes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      checked: false
    };
  }

  handleChange = name => event => {
    this.setState({
      [name]: event.target.checked
    });
  };

  showList = (TeacherSpeakesList)=>{
    const { classes } = this.props
    const { checked } = this.state;
    let result = null
    if(TeacherSpeakesList.length > 0){
      result = TeacherSpeakesList.map ( (item,index) => {
        return (
        <MenuItem className={classes.itemMenu} key={index}>
          <Checkbox
            checked={checked}
            onChange={this.handleChange("checked")}
            value="checked"
            color="primary"
            inputProps={{
              "aria-label": "secondary checkbox"
            }}
            classes={{ root: classes.checkbox }}
          />
          {item}
        </MenuItem>
        )
      })
    }
    return result
  }

  render() {
    const { classes } = this.props;
    return (
      <MenuList className={classes.dropdownSearch}>
        <Scrollbars style={{ height: 350 }}>
          <div className={classes.inputSearch}>
            <SearchIcon className={classes.iconSearch} />
            <input
              type="search"
              name="q"
              placeholder="Search..."
              className={classes.input}
            />
          </div>
          <Typography
            variant="h6"
            component="h2"
            className={classes.textSearch}
          >
            Popular
          </Typography>
            {this.showList(TeacherSpeakesList)}
        </Scrollbars>
      </MenuList>
    );
  }
}

export default withStyles(styles)(TeacherSpeakes);

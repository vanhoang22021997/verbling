/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import MenuList from "@material-ui/core/MenuList";

import SearchIcon from "@material-ui/icons/Search";

import styles from "./styles";

class Search extends Component {

  handleChange = name => event => {
    this.setState({
      [name]: event.target.checked
    });
  };

  render() {
    const { classes } = this.props;
    return (
      <MenuList className={classes.dropdownSearch}>
          <div className={classes.inputSearch}>
            <SearchIcon className={classes.iconSearch} />
            <input
              type="search"
              name="q"
              placeholder="Search..."
              className={classes.input}
            />
          </div>
      </MenuList>
    );
  }
}

export default withStyles(styles)(Search);

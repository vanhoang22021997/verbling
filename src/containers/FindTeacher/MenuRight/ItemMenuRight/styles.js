const styles = () => ({
  item:{
    position : "relative",
    padding: "0 5px"
  },
  button: {
    display: "block",
    backgroundColor: "white",
    border: "none",
    textAlign: "center",
    cursor: "pointer",
    "&::before":{
      content: `""`,
      width: "0",
      height: "2px",
      backgroundImage: "linear-gradient(116deg,#0699cd,#3bb85c)",
      position: "absolute",
      bottom: "-6px",
      left: "0",
      transition: "all .2s ease-in-out 0s",
    },
    "&:focus":{
      outline: "none",
      "&::before":{
        width: "100%",
      },
    }
  },
  textButton: {
    fontSize: "12.8px",
    letterSpacing: "-.61px",
    color: "#838789",
    fontWeight: "700",
    display: "flex",
    alignItems: "center"
  },
  iconImg:{
    marginRight: "10px"
  },
  "@media only screen and (max-width: 992px)":{
    "item":{
      display: "none"
    }
}
});

export default styles;

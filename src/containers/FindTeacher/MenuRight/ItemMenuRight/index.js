/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";

import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";

import Typography from "@material-ui/core/Typography";

import styles from "./styles";

class ItemMenuRight extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMenuDropdown: false
    };
  }

  showMenu = () => {
    this.setState({ showMenuDropdown: true }, () => {
      document.addEventListener("click", this.closeMenu);
    });
  };
  closeMenu = (e) => {
    if (!this.dropdownMenu.contains(e.target)) {
      this.setState({ showMenuDropdown: false }, () => {
        document.removeEventListener("click", this.closeMenu);
      });
    }
  };

  render() {
    const { showMenuDropdown } = this.state;
    const { classes, menu } = this.props;
    return (
      <div className={classes.item}
      ref={(element) => {
        this.dropdownMenu = element;
      }}
      >
        <button className={classes.button} onClick={this.showMenu}>
          <img
            width="24px"
            height="24px"
            alt="talk_female"
            src={menu.img}
            aria-hidden="true"
            className={classes.iconImg}
          />
          <Typography
            variant="h6"
            component="h2"
            className={classes.textButton}
          >
            {menu.title}
            {showMenuDropdown ? (<ArrowDropUp fontSize="small" className={classes.iconBtn} />) : (<ArrowDropDown fontSize="small" className={classes.iconBtn}/>)}
            
          </Typography>
        </button>
        {showMenuDropdown ? menu.menuDropdown : null}
      </div>
    );
  }
}

export default withStyles(styles)(ItemMenuRight);

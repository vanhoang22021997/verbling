const styles = () => ({
  item: {
    display: "none"
  },
  textBtn: {
    color: "#0699cd",
    fontSize: "14.25px",
    textTransform: "none",
    borderRadius: "8px",
    border: "1px solid #e6e6e6",
    height: "56px"
  },
  rootBtn: {
    width: "110px",
    "&:hover": {
      background: "none !important",
      color: "#20b3e7 !important",
    }
  },
  textBtnClose:{
    color: "#0699cd",
    fontSize: "14.25px",
    border: "2px solid #0699cd",
    fontWeight: "600",
  },
  title:{
    background: "#f8f8f8",
    fontSize: "700",
    borderBottom: "1px solid #d8d8d8",
  },
  footer:{
    display: "flex",
    justifyContent: "space-between",
  },
  rootBtnClose: {
    "&:hover": {
      background: "#0699cd !important",
      color: "white !important",
    }
  },
  textBtnAplly:{
    color: "white",
    background: "#0699cd",
    fontSize: "14.25px",
    border: "2px solid #0699cd",
    fontWeight: "600",
  },
  rootBtnAplly: {
    "&:hover": {
      background: "#05749b !important",
      color: "white !important",
    }
  },
  price:{
    display: "flex",
    alignItems: "center"
  },
  rootContent:{
    background: "white",
    height: "70px"
  },
  slider: {
    width: "80%",
    margin: "0 auto"
  },
  imgChart:{
    marginLeft: "10px"
  },
  root:{
    color: "#3bb85c",
    height: "5px",
    position: "absolute",
    left: "7px",
    bottom: "0",
    width: "93%"
  },
  thumb:{
    backgroundColor: "white",
    border: "1px solid #e6e6e6",
    boxShadow: "0 2px 2px rgba(72,72,72,.3)",
    width: "30px",
    height: "30px",
    top: "5%"
  },
  textTitleA:{
    fontSize: "14.8px",
    color: "#838789",
    letterSpacing: "-.61px",
    fontWeight: 700,
    marginBottom: "0px"
  },
  textTitleB:{
    color: "#999",
    fontWeight: 400,
    fontSize: "12.25px",
    cursor: "not-allowed",
  },
  textPrice:{
    color: "#4f5457",
    cursor: "not-allowed",
  },
  "@media only screen and (max-width: 992px)": {
    item: {
      display: "block"
    }
  }
});

export default styles;

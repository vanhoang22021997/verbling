/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { withStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import ArrowDropDown from "@material-ui/icons/ArrowDropDown";
import ArrowDropUp from "@material-ui/icons/ArrowDropUp";
import Typography from "@material-ui/core/Typography";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Slider from "@material-ui/core/Slider";


import styles from "./styles";

const ExpansionPanel = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    },
    "&$expanded": {
      margin: "auto"
    }
  },
  expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56
    }
  },
  content: {
    "&$expanded": {
      margin: "12px 0"
    }
  },
  expanded: {}
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2)
  }
}))(MuiExpansionPanelDetails);

class ItemMenuResponsive extends Component {
  state = {
    open: false,
    expanded: "",
    value: [117000 , 1750000]
  };

  handleSliderChange = (event, newValue) => {
    this.setState({
      value: newValue
    });
  };

  handleChange = panel => (event, newExpanded) => {
    this.setState({
      expanded: newExpanded ? panel : false
    });
  };

  handleClickOpen = () => {
    this.setState({
      open: true
    });
  };

  handleClose = () => {
    this.setState({ open: false });
  };
  render() {
    const { classes } = this.props;
    const { open, expanded, value } = this.state;
    return (
      <div className={classes.item}>
        <Button
          variant="text"
          color="default"
          className={classes.button}
          disableRipple
          disableFocusRipple={true}
          classes={{ text: classes.textBtn, root: classes.rootBtn }}
          onClick={this.handleClickOpen}
        >
          <img
            width="30px"
            height="30px"
            alt="vertical_settings_mixer"
            src="https://cdn.verbling.com/static/svg/icons8/42c1d518dfe99f30006739e372034121.icons8-vertical_settings_mixer.svg"
            className="margin-right-sm"
          />
          Filters
        </Button>
        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="draggable-dialog-title"
        >
          <DialogTitle
            style={{ cursor: "auto" }}
            id="draggable-dialog-title"
            classes={{ root: classes.title }}
          >
            Filters
          </DialogTitle>
          <DialogContent>
            <DialogContentText>
              <ExpansionPanel
                square
                expanded={expanded === "panel1"}
                onChange={this.handleChange("panel1")}
              >
                <ExpansionPanelSummary
                  aria-controls="panel1d-content"
                  id="panel1d-header"
                  classes={{ root: classes.rootContent }}
                >
                  <div className={classes.price}>
                    <img
                      width="24px"
                      height="24px"
                      alt="banknotes"
                      src="https://cdn.verbling.com/static/svg/icons8/f1646c39d432f286762241f9514117f4.icons8-banknotes.svg"
                      aria-hidden="true"
                    />
                    <div style={{ marginLeft: "10px", width: "400px" }}>
                      <Typography className={classes.textTitleA}>Price</Typography>
                      <Typography className={classes.textPrice}>398.63 ₫-5,979.41 ₫</Typography>
                    </div>
                    {expanded === "panel1" ? <ArrowDropDown className={classes.iconArrow}/>: <ArrowDropUp className={classes.iconArrow}/>}
                  </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <img
                    src="/img/chartprice.jpg"
                    alt="chart"
                    className={classes.imgChart}
                  />
                  <div className={classes.slider}>
                    <Slider
                      classes={{
                        root: classes.root,
                        thumb: classes.thumb,
                      }}
                      value={value}
                      onChange={this.handleSliderChange}
                      aria-labelledby="range-slider"
                      min={117000}
                      max={1750000}
                    />
                  </div>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <ExpansionPanel
                square
                expanded={expanded === "panel2"}
                onChange={this.handleChange("panel2")}
              >
                <ExpansionPanelSummary
                  aria-controls="panel2d-content"
                  id="panel2d-header"
                  classes={{ root: classes.rootContent }}
                >
                    <div className={classes.price}>
                    <img width="24px" height="24px" alt="calendar" src="https://cdn.verbling.com/static/svg/icons8/8110dc645330172da02b881d21d8ee28.icons8-calendar.svg" aria-hidden="true"/>
                    <div style={{ marginLeft: "10px", width: "400px" }}>
                      <Typography className={classes.textTitleA}>Availability</Typography>
                      <Typography className={classes.textTitleB}>Select availability</Typography>
                    </div>
                    {expanded === "panel2" ? <ArrowDropDown className={classes.iconArrow}/>: <ArrowDropUp className={classes.iconArrow}/>}
                  </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Suspendisse malesuada lacus ex, sit amet
                    blandit leo lobortis eget.
                  </Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <ExpansionPanel
                square
                expanded={expanded === "panel3"}
                onChange={this.handleChange("panel3")}
              >
                <ExpansionPanelSummary
                  aria-controls="panel3d-content"
                  id="panel3d-header"
                  classes={{ root: classes.rootContent }}
                >
                      <div className={classes.price}>
                      <img width="24px" height="24px" alt="add_to_favorites" src="https://cdn.verbling.com/static/svg/icons8/1f97fa260915623eb6f064e265046086.icons8-add_to_favorites.svg" aria-hidden="true" />
                    <div style={{ marginLeft: "10px", width: "400px" }}>
                      <Typography className={classes.textTitleA}>Skills</Typography>
                      <Typography className={classes.textTitleB}>Select skills</Typography>
                    </div>
                    {expanded === "panel3" ? <ArrowDropDown className={classes.iconArrow}/>: <ArrowDropUp className={classes.iconArrow}/>}
                  </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Suspendisse malesuada lacus ex, sit amet
                    blandit leo lobortis eget.
                  </Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <ExpansionPanel
                square
                expanded={expanded === "panel4"}
                onChange={this.handleChange("panel4")}
              >
                <ExpansionPanelSummary
                  aria-controls="panel4d-content"
                  id="panel4d-header"
                  classes={{ root: classes.rootContent }}
                >
                      <div className={classes.price}>
                      <img width="24px" height="24px" alt="location" src="https://cdn.verbling.com/static/svg/icons8/4921cda723b28e5223706f350a370791.icons8-location.svg" aria-hidden="true" />
                    <div style={{ marginLeft: "10px", width: "400px" }}>
                      <Typography className={classes.textTitleA}>From</Typography>
                      <Typography className={classes.textTitleB}>Select country</Typography>
                    </div>
                    {expanded === "panel4" ? <ArrowDropDown className={classes.iconArrow}/>: <ArrowDropUp className={classes.iconArrow}/>}
                  </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Suspendisse malesuada lacus ex, sit amet
                    blandit leo lobortis eget.
                  </Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
              <ExpansionPanel
                square
                expanded={expanded === "panel5"}
                onChange={this.handleChange("panel5")}
              >
                <ExpansionPanelSummary
                  aria-controls="panel5d-content"
                  id="panel5d-header"
                  classes={{ root: classes.rootContent }}
                >
                      <div className={classes.price}>
                      <img width="24px" height="24px" alt="user_group_man_woman" src="https://cdn.verbling.com/static/svg/icons8/fc07feeb3602d4f4a16447924bcad804.icons8-user_group_man_woman.svg" aria-hidden="true" />
                    <div style={{ marginLeft: "10px", width: "400px" }}>
                      <Typography className={classes.textTitleA}>Gender</Typography>
                      <Typography className={classes.textTitleB}>Select gender</Typography>
                    </div>
                    {expanded === "panel5" ? <ArrowDropDown className={classes.iconArrow}/>: <ArrowDropUp className={classes.iconArrow}/>}
                  </div>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                  <Typography>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Suspendisse malesuada lacus ex, sit amet blandit leo
                    lobortis eget. Lorem ipsum dolor sit amet, consectetur
                    adipiscing elit. Suspendisse malesuada lacus ex, sit amet
                    blandit leo lobortis eget.
                  </Typography>
                </ExpansionPanelDetails>
              </ExpansionPanel>
            </DialogContentText>
          </DialogContent>
          <DialogActions className={classes.footer}>
            <Button
              onClick={this.handleClose}
              color="default"
              classes={{
                text: classes.textBtnClose,
                root: classes.rootBtnClose
              }}
            >
              Close
            </Button>
            <Button
              onClick={this.handleClose}
              color="default"
              classes={{
                text: classes.textBtnAplly,
                root: classes.rootBtnAplly
              }}
            >
              Apply
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(ItemMenuResponsive);

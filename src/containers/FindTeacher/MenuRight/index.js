/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import ItemMenuRight from './ItemMenuRight'
import TeacherSpeakes from './ItemMenuRight/TeacherSpeakes'
import Skills from "./ItemMenuRight/Skills";
import From from './ItemMenuRight/From'
import Gender from './ItemMenuRight/Gender'
import Search from './ItemMenuRight/Search'
import Price from './ItemMenuRight/Price'
import ItemMenuResponsive from './ItemMenuResponsive'

const menusRight = [
  {
    title: "Teacher speakes",
    img: "https://cdn.verbling.com/static/svg/icons8/36ee39b9832072e0483656201c2bdeca.icons8-talk_female.svg",
    menuDropdown: <TeacherSpeakes/>
  },
  {
    title: "Price",
    img: "https://cdn.verbling.com/static/svg/icons8/f1646c39d432f286762241f9514117f4.icons8-banknotes.svg",
    menuDropdown: <Price/>
  },
  {
    title: "Skills",
    img: "https://cdn.verbling.com/static/svg/icons8/1f97fa260915623eb6f064e265046086.icons8-add_to_favorites.svg",
    menuDropdown: <Skills/>
  },
  {
    title: "From",
    img: "https://cdn.verbling.com/static/svg/icons8/4921cda723b28e5223706f350a370791.icons8-location.svg",
    menuDropdown: <From/>
  },
  {
    title: "Gender",
    img: "https://cdn.verbling.com/static/svg/icons8/fc07feeb3602d4f4a16447924bcad804.icons8-user_group_man_woman.svg",
    menuDropdown: <Gender/>
  },
  {
    title: "Search",
    img: "https://cdn.verbling.com/static/svg/icons8/8548a99c61b1b3131074a98251773b3b.icons8-search.svg",
    menuDropdown: <Search/>
  }
]

class MenuRight extends Component {

  showMenuRight = (menus) => {
    let result = null
    if(menus.length > 0) {
      result = menus.map( (menu, index) => {
        return <ItemMenuRight key = {index} menu = {menu}/> 
      })
    }
    return result
  }

  render() {
    const { classes } = this.props;

    return (
          <Grid
            item
            xs={6}
            container
            className={classes.grid}
            justify="flex-end"
          >
            {this.showMenuRight(menusRight)}
            <ItemMenuResponsive/>
        </Grid>
    );
  }
}

export default withStyles(styles)(MenuRight);

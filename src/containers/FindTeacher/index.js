/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Grid } from "@material-ui/core";
import { withStyles } from "@material-ui/styles";
import styles from "./styles";
import MenuRight from "./MenuRight";
import MenuLeft from "./MenuLeft";
import clsx from "clsx";

class FindTeacher extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrolling: false
    };
  }

  componentDidMount = () => {
    window.addEventListener("scroll", this.handleScroll);
  };

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.handleScroll);
  };

  handleScroll = e => {
    if (window.scrollY > 100) {
      this.setState({
        scrolling: true
      });
    } else {
      this.setState({
        scrolling: false
      });
    }
  };
  render() {
    const { classes } = this.props;
    const { scrolling } = this.state;
    return (
      <div
        className={clsx(classes.findTeacher, scrolling ? classes.fixed : null)}
      >
        <Grid className={classes.container}>
          <Grid container spacing={2} className={classes.menu}>
            <MenuLeft />
            <MenuRight />
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(FindTeacher);

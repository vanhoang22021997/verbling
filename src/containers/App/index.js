import React, { Component } from "react";
import styles from "./styles";
import { ThemeProvider, withStyles } from "@material-ui/styles";
import Header from "./../Header";
import FindTeacher from "./../FindTeacher";
import theme from "./../../commons/theme";
import ListingTeacher from "../ListingTeacher";
import Footer from "./../Footer";
class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scrolling: false
    };
  }
  render() {
    return (
      <ThemeProvider theme={theme}>
        <Header />
        <FindTeacher/>
        <ListingTeacher />
        <Footer />
      </ThemeProvider>
    );
  }
}

export default withStyles(styles)(App);
